/*
 * doomgeneric for Loko Scheme with Valand
 *
 * Copyright © 2022 Göran Weinholt
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 *
 **/

#include "doomkeys.h"
#include "doomgeneric.h"

#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <time.h>
#include <unistd.h>

static int valandfd;
static unsigned char *buffer;
static const size_t buffer_size = DOOMGENERIC_RESY * DOOMGENERIC_RESX * sizeof(uint32_t);

struct valand_simple_surface {
    uint16_t resx;
    uint16_t resy;
    uint32_t format;
};

struct valand_event {
    int64_t time;
    uint16_t page;
    uint16_t usage;
    int32_t ch;
    int pressed;
    uint32_t pad[3];
};

struct valand_get_events {
    int n;
    struct valand_event events[8];
};

#define fourcc(x,y,z,w) ((x)|((y)<<8)|((z)<<16)|((w)<<24))
#define VALAND_IOCTL_MAGIC 'v'
#define VALAND_SIMPLE_SURFACE      _IOW(VALAND_IOCTL_MAGIC, 1, struct valand_simple_surface)
#define VALAND_DAMAGE_FULL_SURFACE _IOW(VALAND_IOCTL_MAGIC, 2, uint32_t)
#define VALAND_GET_EVENTS          _IOR(VALAND_IOCTL_MAGIC, 3, struct valand_get_events)
#define VL_FORMAT_XRGB8888         fourcc('X','R','2','4')

void DG_Init() {
    int err;

    valandfd = open("/dev/valand", O_RDWR, 0);
    if (valandfd < 0) {
        perror("open");
        exit(1);
    }

    struct valand_simple_surface surface = {
        .resx = DOOMGENERIC_RESX,
        .resy = DOOMGENERIC_RESY,
        .format = VL_FORMAT_XRGB8888,
    };

    err = ioctl(valandfd, VALAND_SIMPLE_SURFACE, &surface);
    if (err) {
        perror("ioctl");
        exit(1);
    }

    buffer = mmap(NULL, buffer_size, PROT_READ|PROT_WRITE, MAP_SHARED, valandfd, 0);
    if (!buffer) {
        perror("mmap");
        exit(1);
    }
}

void DG_DrawFrame()
{
    memcpy(buffer, DG_ScreenBuffer, buffer_size);
    ioctl(valandfd, VALAND_DAMAGE_FULL_SURFACE);
}

void DG_SleepMs(uint32_t ms)
{
    uint64_t s = ms / 1000;
    uint64_t nsec = (ms % 1000) * 1000000ULL;

    const struct timespec ts = {
        .tv_sec = s,
        .tv_nsec = nsec,
    };

    nanosleep(&ts, NULL);
}

uint32_t DG_GetTicksMs()
{
    struct timespec ts;

    clock_gettime(CLOCK_REALTIME, &ts);

    return ts.tv_sec * 1000 + ts.tv_nsec / 1000000;
}

int DG_GetKey(int* pressed, unsigned char* doomKey)
{
    int n;
    struct valand_event ev;

    n = read(valandfd, &ev, sizeof ev);
    if (n < 0) {
        if (errno == EAGAIN) {
            return 0;
        }

        perror("read");
        exit(1);
    }

    *pressed = ev.pressed;

    switch ((ev.page << 16) | ev.usage) {
    /* wasd */
    case 0x0007001a: *doomKey = KEY_UPARROW; return 1;
    case 0x00070004: *doomKey = KEY_STRAFE_L; return 1;
    case 0x00070016: *doomKey = KEY_DOWNARROW; return 1;
    case 0x00070007: *doomKey = KEY_STRAFE_R; return 1;

    case 0x00070028: *doomKey = KEY_ENTER; return 1;
    case 0x0007002a: *doomKey = KEY_BACKSPACE; return 1;
    case 0x0007002b: *doomKey = KEY_TAB; return 1;
    case 0x0007002c: *doomKey = KEY_USE; return 1; /* spacebar */
    case 0x0007003a: *doomKey = KEY_F1; return 1;
    case 0x0007003b: *doomKey = KEY_F2; return 1;
    case 0x0007003c: *doomKey = KEY_F3; return 1;
    case 0x0007003d: *doomKey = KEY_F4; return 1;
    case 0x0007003e: *doomKey = KEY_F5; return 1;
    case 0x0007003f: *doomKey = KEY_F6; return 1;
    case 0x00070040: *doomKey = KEY_F7; return 1;
    case 0x00070041: *doomKey = KEY_F8; return 1;
    case 0x00070042: *doomKey = KEY_F9; return 1;
    case 0x00070043: *doomKey = KEY_F10; return 1;
    case 0x00070044: *doomKey = KEY_F11; return 1;
    case 0x00070045: *doomKey = KEY_F12; return 1;
    case 0x00070048: *doomKey = KEY_PAUSE; return 1;
    case 0x0007004a: *doomKey = KEY_HOME; return 1;
    case 0x0007004b: *doomKey = KEY_PGUP; return 1;
    case 0x0007004c: *doomKey = KEY_DEL; return 1;
    case 0x0007004d: *doomKey = KEY_END; return 1;
    case 0x0007004e: *doomKey = KEY_PGDN; return 1;
    case 0x0007004f: *doomKey = KEY_RIGHTARROW; return 1;
    case 0x00070050: *doomKey = KEY_LEFTARROW; return 1;
    case 0x00070051: *doomKey = KEY_DOWNARROW; return 1;
    case 0x00070052: *doomKey = KEY_UPARROW; return 1;
    case 0x00070058: *doomKey = KEYP_ENTER; return 1;
    case 0x000700E0: *doomKey = KEY_FIRE; return 1; /* left control */
    case 0x000700E1: *doomKey = KEY_RSHIFT; return 1; /* left shift */
    case 0x000700E2: *doomKey = KEY_LALT; return 1;
    case 0x000700E4: *doomKey = KEY_FIRE; return 1; /* right control */
    case 0x000700E5: *doomKey = KEY_RSHIFT; return 1;
    case 0x000700E6: *doomKey = KEY_RALT; return 1;

    default:
        if (ev.ch != -1 && ev.ch < 0x80) {
            *doomKey = ev.ch;
            return 1;
        }

        return 0;
    }

    return 0;
}

void DG_SetWindowTitle(const char * title)
{
}
